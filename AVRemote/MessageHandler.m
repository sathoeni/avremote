//
//  CommandHandler.m
//  AVRemote
//
//  Created by Sascha Thöni on 30.04.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import "MessageHandler.h"

@implementation MessageHandler

- (ReceiverState*) handle:(NSString *)dataString
{
    NSLog(@"handleString: %@", dataString);
    
    NSString * result = @"Unhandled Datat";
    
    // Decide how the string has to be handled
    if ([dataString hasPrefix:@"FL"])
    {
        NSString* screenMessage = [self handleScreenMessage: dataString];
        [[ReceiverState getInstance] setScreenMessage:screenMessage];
    }
    else if([dataString hasPrefix:@"VOL"])
    {
        int volume = [self handleVolume: dataString];
        [[ReceiverState getInstance] setVolume:volume];
    }
    
    return [ReceiverState getInstance];
}



- (int) handleVolume: (NSString*) rawString
{
    NSString* volumeString = [rawString substringWithRange:NSMakeRange(3, 3)];
    int volume = [volumeString intValue];
    NSLog(@"Volume: %d", volume);
    return volume;
}


- (NSString*) handleScreenMessage:(NSString*) dataString
{
    NSString* screenTextAsciiString = @"";
    // * split the hex-string into single hex-numbers, convert the hex-string-number into an ascii-character
    // * and add it to the complete string
    // * We start from i = 2 because we don't wanna parse the "FL" at the beginning of the string
    for(int i = 2; i < (dataString.length - 2); )
    {
        
        //NSLog(@"intValue: %i", i);
        
        
        NSString* hexString = [dataString substringWithRange:NSMakeRange(i, 2)];
        //NSLog(@"ASCII-HEX_VALUE: %@", hexString);
        
        
        int intValueOfHexString = 0;
        sscanf([hexString UTF8String], "%x", &intValueOfHexString);
        
        //NSLog(@"ASCII-DEC_VALUE: %d", intValueOfHexString);
        
        // * only accept ascii-value that are usefull.
        // * see: http://www.torsten-horn.de/techdocs/ascii.htm
        
        if(intValueOfHexString > 31 && intValueOfHexString < 127)
        {
            
            NSString *asciiString = [NSString stringWithFormat:@"%c", intValueOfHexString];
            //NSLog(@"asciisctring: %@", asciiString);
            
            screenTextAsciiString = [screenTextAsciiString stringByAppendingString:asciiString];
        }
        
        
        i = i + 2;
    }
    
    NSLog(@"screenTextAsciiString: %@", screenTextAsciiString);
    
    
    //NSLog(@"Read %d byte(s): %@", result, dataString);
    NSLog(@"------------------");
    
    [[ReceiverState getInstance] setScreenMessage:screenTextAsciiString];
    
    return screenTextAsciiString;

}
@end
