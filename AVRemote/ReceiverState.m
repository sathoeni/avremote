//
//  ReceiverState.m
//  AVRemote
//
//  Created by Sascha Thöni on 07.06.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import "ReceiverState.h"

@implementation ReceiverState
@synthesize screenMessage;
@synthesize volume;

+ (id)getInstance {
    static ReceiverState *receiverState = nil;
    @synchronized(self) {
        if (receiverState == nil)
            receiverState = [[self alloc] init];
    }
    return receiverState;
}
/*
- (void) setVolume:(int)localVolume
{
    volume = localVolume;
}
*/
- (int) getVolume
{
    return volume;
}

- (NSString*) getScreenMessage
{
    return screenMessage;
}
@end
