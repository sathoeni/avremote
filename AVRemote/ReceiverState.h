//
//  ReceiverState.h
//  AVRemote
//
//  Created by Sascha Thöni on 07.06.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReceiverState : NSObject

+ (id)getInstance;
- (NSString*) getScreenMessage;
- (int) getVolume;
//- (void) setVolume: (int) volume;

@property (nonatomic, strong) NSString *screenMessage;
@property int volume;
@end
