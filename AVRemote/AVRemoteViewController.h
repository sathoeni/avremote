//
//  AVRemoteViewController.h
//  AVRemote
//
//  Created by Sascha Thöni on 28.04.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AVRemote.h"

@interface AVRemoteViewController : UIViewController

@property (strong) AVRemote *avRemote;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *muteButton;

@property (strong, nonatomic) IBOutlet UILabel *statusLabel;

@property (strong, nonatomic) IBOutlet UISlider *volumeSlider;
- (IBAction)mute:(id)sender;
- (IBAction)volumeChanged:(id)sender;

@end
