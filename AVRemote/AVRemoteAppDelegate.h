//
//  AVRemoteAppDelegate.h
//  AVRemote
//
//  Created by Sascha Thöni on 28.04.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AVRemoteViewController;

@interface AVRemoteAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) AVRemoteViewController *viewController;

@end
