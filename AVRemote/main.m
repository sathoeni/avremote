//
//  main.m
//  AVRemote
//
//  Created by Sascha Thöni on 28.04.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AVRemoteAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AVRemoteAppDelegate class]));
    }
}
