//
//  CommandHandler.h
//  AVRemote
//
//  Created by Sascha Thöni on 30.04.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReceiverState.h"
@interface MessageHandler : NSObject

- (ReceiverState*) handle:(NSString*) dataString;

@end
