//
//  AVRemoteViewController.m
//  AVRemote
//
//  Created by Sascha Thöni on 28.04.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import "AVRemoteViewController.h"

@interface AVRemoteViewController ()

@end

@implementation AVRemoteViewController

@synthesize avRemote = _avRemote;
@synthesize volumeSlider = _volumeSlider;
@synthesize statusLabel;


- (id) init
{
    self = [super init];
    if (!self) return nil;
    
    
    //TODO remove from here and move to init
    //[statusLabel setFont:[UIFont fontWithName:@"DBLCDTempBlack" size:12.0f]];
    
	// Do any additional setup after loading the view, typically from a nib.
    [self runReceiver];
    
    // Add this instance of TestClass as an observer of the TestNotification.
    // We tell the notification center to inform us of "TestNotification"
    // notifications using the receiveTestNotification: selector. By
    // specifying object:nil, we tell the notification center that we are not
    // interested in who posted the notification. If you provided an actual
    // object rather than nil, the notification center will only notify you
    // when the notification was posted by that particular object.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self init];
    
    //SET THE FRAME ACCORDING TO YOUR USE
    //myLabel=[[UILabel alloc]initWithFrame:CGRectMake(10,10,200,40)];
    //[self.view addSubview:myLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) runReceiver
{
    NSLog(@"runReceiver");
    AVRemote *aAVRemote= [[AVRemote alloc] init];
    [self setAvRemote:aAVRemote];
    [self.avRemote initReceiver:nil];
}   

- (IBAction)mute:(id)sender {
    [self.avRemote mute];
}

- (IBAction)volumeChanged:(id)sender {
    float volume = [self.volumeSlider value];
    [self.avRemote setVolume:volume];
    //NSLog(@"volume %f", volume);
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    if ([[notification name] isEqualToString:@"TestNotification"])
        NSLog (@"Successfully received the test notification!");
    
        //Receiving data is no more necessary, because UI data is stored in singleton
        //NSDictionary *userInfo = notification.userInfo;
        //NSString *statusText  = [userInfo objectForKey:@"statusText"];
    
    //update the ui (when messageHandler sends changed-Notification)
    //performSelectorOnMainThread is necessary, otherwhise the UI will not update
    [self performSelectorOnMainThread:@selector(updateUI:) withObject:nil waitUntilDone:NO];
}

- (void) updateUI:(id)id
{
    //[dict setObject:displayString forKey:@"statusText"];
    [self updateStatusLabel:[[ReceiverState getInstance] getScreenMessage]];
    [self updateVolume:[[ReceiverState getInstance] volume]];
    
    NSString* screenMessage = [[ReceiverState getInstance] getScreenMessage];
    NSLog(@"ScreenMessage: %@", screenMessage);
}

- (void) updateStatusLabel:(NSString*) statusText
{
    statusLabel.text = statusText;
    [statusLabel setNeedsDisplay];
}

- (void) updateVolume: (int) volume
{
    [self.volumeSlider setValue:volume];
}

- (void) dealloc
{
    // If you don't remove yourself as an observer, the Notification Center
    // will continue to try and send notification objects to the deallocated
    // object.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //[super dealloc];
}
@end
