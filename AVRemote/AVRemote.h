//
//  AVRemote.h
//  AVRemote
//
//  Created by Sascha Thöni on 28.04.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageHandler.h"
#import "AppConfig.h"


@interface AVRemote : NSObject<NSStreamDelegate>

@property FILE *pipe;
@property NSString *statusText;
@property (strong) NSInputStream *inputStream;
@property (strong) NSOutputStream *outputStream;
@property (strong) MessageHandler *messageHandler;

// Receiver I/O Methods
- (void) initReceiver:(id)param;

//Controlling Methods
- (void) mute;
- (void) setVolume:(double) volume;
- (void) controllClickLeft;
- (void) controllClickRight;
- (void) controllClickUp;
- (void) controllClickDown;




- (NSString*) getStatusText;



@end
