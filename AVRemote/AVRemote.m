//
//  AVRemote.m
//  AVRemote
//
//  Created by Sascha Thöni on 28.04.13.
//  Copyright (c) 2013 Sascha Thöni. All rights reserved.
//

#import "AVRemote.h"

@implementation AVRemote

@synthesize pipe;
@synthesize statusText;
@synthesize inputStream;
@synthesize outputStream;
@synthesize messageHandler;



- (void) initReceiver:(id)param
{    
    NSLog(@"init Receiver");
    
    messageHandler = [[MessageHandler alloc] init];
    
    /* open receiver connection */
    [self initNetworkCommunication];
    
    
    //[self readContinousFromPipe:nil];
    [NSThread detachNewThreadSelector:@selector(readContinousFromPipe:) toTarget:self withObject:nil];

}

- (void) readContinousFromPipe:(id)param
{
    NSInteger result;
    
    int bufferSize = 34;
    uint8_t buffer[bufferSize];
    NSString *dataString;

    while((result = [inputStream read:buffer maxLength:bufferSize]) != 0)
    {
        usleep(100);
        if(result > 0)
        {
         
            // buffer contains result bytes of data to be handled
            //dataString = [[NSString alloc] initWithBytes:buffer length:bufferSize encoding:NSUTF8StringEncoding];
            dataString = [[NSString alloc] initWithBytes:buffer length:bufferSize encoding:NSASCIIStringEncoding];
        
            ReceiverState *receiverState = [messageHandler handle:dataString];
            self.statusText = receiverState.getScreenMessage;
            
            //NSString *intString = [NSString stringWithFormat:@"%d", i];
            //self.statusText = intString;
            
            //no more necessary to send the ReceiverState by notification, because it is singleton
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            //[dict setObject:displayString forKey:@"statusText"];
            //[dict setObject:[ReceiverState getInstance] forKey:@"receiverState"];
            //NSDictionary *userInfo = [NSDictionary dictionaryWithObject:displayString forKey:@"statusText"];
            //TODO remove comments and dict from sending with notification
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TestNotification" object:self userInfo:dict];
        }
        else
        {
            // The stream had an error. You can get an NSError object using [iStream streamError]
            NSError* error = [inputStream streamError];
            NSLog(@"Error %@",[error localizedDescription]);
        }
    }
}


//--------------------------------------------------------------------
// Controlling Methods
//--------------------------------------------------------------------

- (void) sendCommand:(NSString*)command
{
    NSData *data = [[NSData alloc] initWithData:[command dataUsingEncoding:NSUTF8StringEncoding]];
    [outputStream write:[data bytes] maxLength:[data length]];
}


- (void) mute
{    
    [self setVolume:54.0];
}

- (void) setVolume:(double)volume
{
    //printf("volume: %f \n",volume);
    
    NSString *volumeString = [NSString stringWithFormat:@"%ld",lroundf(volume)];
    
    //if volume is smaller then then 10, add en extra 0 to the command,
    // because commands schould always look like: xxxVL e.g. 004VL, 034VL
    if(volume < 10)
    {
        volumeString = [@"0" stringByAppendingString:volumeString];
    }
    else if(volume > 10 && volume < 100)
    {
        volumeString = [@"0" stringByAppendingString:volumeString];
    }
    volumeString = [volumeString stringByAppendingString:@"VL\r\n"];
    NSLog( @"setVolume: %@", volumeString );
    
    [self sendCommand:volumeString];
}



- (void) logStreamStatus
{
    NSStreamStatus outputStreamStatus = [outputStream streamStatus];
    NSNumber* outputStreamStatusNumber = [NSNumber numberWithInt:outputStreamStatus];
    NSLog(@"OutputStreamStatus %d", [outputStreamStatusNumber integerValue]);
    
    NSStreamStatus streamStatus = [inputStream streamStatus];
    NSNumber* streamStatusNumber = [NSNumber numberWithInt:streamStatus];
    NSLog(@"InputStreamStatus %d", [streamStatusNumber integerValue]);

}

- (void)initNetworkCommunication {
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)IP_ADRESS, 23, &readStream, &writeStream);
    
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    
    
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [inputStream open];
    [outputStream open];
    
    NSError* error = [inputStream streamError];
    NSLog(@"Error %@",[error localizedDescription]);
    
    NSStreamStatus streamStatus = [inputStream streamStatus];
    NSNumber* streamStatusNumber = [NSNumber numberWithInt:streamStatus];
    NSLog(@"InputStreamStatus %d", [streamStatusNumber integerValue]);
}



- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    NSLog(@"stream event %i", streamEvent);

	switch (streamEvent) {
            
		case NSStreamEventOpenCompleted:
			NSLog(@"Stream opened");
			break;
            
		case NSStreamEventHasBytesAvailable:
			break;
            
		case NSStreamEventErrorOccurred:
			NSLog(@"Can not connect to the host!");
			break;
            
		case NSStreamEventEndEncountered:
			break;
            
		default:
			NSLog(@"Unknown event");
	}
    
}


- (NSString*) getStatusText
{
    return self.statusText;
}


















/*
- (void) readContinousFromPipe:(id)param
{
    //variables for fgets()
    char buffer[256];
    
    // make the fgets()-call non-blocking 
    int fd = fileno(pipe);
    int flags = fcntl(fd, F_GETFL, 0);
    flags |= O_NONBLOCK;
    fcntl(fd, F_SETFL, flags);
    
    NSLog(@"readContinousFromPipe");
    while(true)
    {
        
        usleep(100);
        
        if(fgets(buffer, sizeof(buffer), pipe))
        {
            
            NSString* inputSourceString = [NSString stringWithUTF8String:buffer];
            const char* inputSourceChar = [inputSourceString UTF8String];
            printf("readContinousFromPipe --------->>>>>>> %s", inputSourceChar);
            
            
            // check if string contains another string
            Boolean volumeFound = !([inputSourceString rangeOfString:@"VOL"].location == NSNotFound);
            Boolean screenTextFound = !([inputSourceString rangeOfString:@"FL"].location == NSNotFound);
            
            if(volumeFound)
            {
                //NSLog(@"getReceiverVolume: %@", inputSourceString);
                
                
                NSString* volumeString = [inputSourceString substringFromIndex:3];
                
                int volume = [volumeString intValue];
                
                //[self.track setVolume:volume];
                
                
            }
            else if(screenTextFound)
            {
                
                //NSLog(@"screenText: %@", inputSourceString);
                
                //cut the "FL" from the string
                //NSString* screenTextHexString = [inputSourceString substringWithRange:NSMakeRange(2, inputSourceString.length - 2)];
                
                
                NSString* screenTextAsciiString = @"";
                
                
                 / * split the hex-string into single hex-numbers, convert the hex-string-number into an ascii-character
                 / * and add it to the complete string
                 / * We start from i = 2 because we don't wanna parse the "FL" at the beginning of the string
                 
                for(int i = 2; i < (inputSourceString.length - 2); )
                {
                    
                    //NSLog(@"intValue: %i", i);
                    
                    
                    NSString* hexString = [inputSourceString substringWithRange:NSMakeRange(i, 2)];
                    //NSLog(@"ASCII-HEX_VALUE: %@", hexString);
                    
                    
                    int intValueOfHexString = 0;
                    sscanf([hexString UTF8String], "%x", &intValueOfHexString);
                    
                    //NSLog(@"ASCII-DEC_VALUE: %d", intValueOfHexString);
                    
                    
                    
                     / * only accept ascii-value that are usefull.
                     / * see: http://www.torsten-horn.de/techdocs/ascii.htm
                     
                    if(intValueOfHexString > 31 && intValueOfHexString < 127)
                    {
                        
                        NSString *asciiString = [NSString stringWithFormat:@"%c", intValueOfHexString];
                        //NSLog(@"asciisctring: %@", asciiString);
                        
                        screenTextAsciiString = [screenTextAsciiString stringByAppendingString:asciiString];
                    }
                    
                    
                    i = i + 2;
                }
                
                NSLog(@"screenTextAsciiString: %@", screenTextAsciiString);
                
                statusText = screenTextAsciiString;
                
                //NSLog([@"1234567890" substringWithRange:NSMakeRange(3, 5)]);
                
                
            }
            
            //[self updateUserInterface];
            
            // 32 Zeichen Total
            // bei zeichen 28
            
        }
    }
}


*/


@end
